import os
import shutil

# Directory paths
input_dir = '../test/matrices'
output_dir = '../test/final_matrices'

# Create the output directory if it doesn't exist
if not os.path.exists(output_dir):
    os.makedirs(output_dir)

# Loop through each file in the input directory
for filename in os.listdir(input_dir):
    # Check if the file is a .mtx file
    if filename.endswith('.mtx'):
        # Generate the input file using GenerateInput.py
        input_file = os.path.join(input_dir, filename)
        os.system(f'python GenerateInput.py {input_file}')


# Loop through each file in the input directory
for filename in os.listdir(input_dir):
    # Check if the file is a .in file
    if filename.endswith('.in'):
        # Move the file to the output directory
        input_file = os.path.join(input_dir, filename)
        shutil.move(input_file, os.path.join(output_dir, filename))

print('Done')
